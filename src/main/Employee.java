package main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

public class Employee {
	private String firstName;
	private String lastName;
	private String email;
	private String ID;
	private String ani; // Caller Number on screen pop

	private Connection con;
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	public Employee(String uui) throws IOException {

		String tempData[];

		/*
		 * Split string by ";" delimiter
		 * If uui only includes a ";" or is empty, assign tempdata [Invalid, Invalid]
		 * else tempData = [{ID}, {ANI}]
		 */
		tempData = uui.equals(";") || uui.equals("") ? new String[] { "Invalid", "Invalid" } : uui.split(";");

		// If tempData[0] is empty or undefined, assign "Invalid", else assign value
		ID = tempData[0].equals("") || tempData[0].equals("undefined") ? "Invalid" : tempData[0];

		// If tempData has an array length of 1 (Ani empty value) or undefined (IVR unable to pass in ANI data to UUI),
		// assign second value as "Invalid", else assign second value of tempData array
		ani = tempData.length == 1 || tempData[1].equals("undefined") ? "Invalid" : tempData[1];

		try {
			getEmployeeInfo();
		} catch (SQLException err) { // compiled to be Java 1.6 compliant
			err.printStackTrace();
			SQLErrorValues(err);

		} catch (ClassNotFoundException err) {
			err.printStackTrace();
			SQLErrorValues(err);
		}
	}

	/*
	 * Method executes for jdbc error / database connection failure.
	 * Logs error message and assign variables error fetching data
	 */
	private void SQLErrorValues(Exception err) throws IOException {
		this.firstName = "Error fetching data!";
		this.lastName = "Error fetching data!";
		this.email = "Error fetching data!";

		final BufferedWriter writer = new BufferedWriter(new FileWriter("SQLError.log"));

		// log file format as "DateTime: SQL Error"
		writer.write(DateFormat.getDateTimeInstance().format(new Date()));
		writer.write(": " + err.toString());

		// close BufferedWriter
		writer.close();
	}

	private void getEmployeeInfo() throws SQLException, ClassNotFoundException {
		
		if (ID.equals("Invalid") == false) {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			DriverManager.setLoginTimeout(2); // if DB cannot be connected within 2 seconds, throws error

			con = DriverManager.getConnection(
					"jdbc:sqlserver://20.195.138.45:1433;databaseName=JJApps_ivr;username=jjadmin;password=jjP@ssw0rd");

			preparedStatement = con.prepareStatement(
					"SELECT [First_name],[Last_name], [Internet_e_mail] FROM [JJApps_ivr].[dbo].[ml_employee] where [Corporate_id] = ?;");
			preparedStatement.setString(1, ID);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				this.firstName = (resultSet.getString("First_name")).equals("") ? "Invalid"
						: resultSet.getString("First_name");
				this.lastName = (resultSet.getString("Last_name")).equals("") ? "Invalid"
						: resultSet.getString("Last_name");
				this.email = (resultSet.getString("Internet_e_mail")).equals("") ? "Invalid"
						: resultSet.getString("Internet_e_mail");

			} else {
				defaultInvalidValues(); // Executes when empty resultset
			}
			con.close();

		} else {
			
			defaultInvalidValues(); // Executes for Invalid ID
		}

	}
	
	// method executed to assign Invalid values when UUI's ID or resultset is null
	private void defaultInvalidValues() {
		this.firstName = "Invalid";
		this.lastName = "Invalid";
		this.email = "Invalid";
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getID() {
		return ID;
	}

	public String getANI() {
		return ani;
	}

}
