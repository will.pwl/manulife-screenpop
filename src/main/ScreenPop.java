package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Image;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import javax.swing.Timer;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class ScreenPop {

	private JFrame frmEmployeeData;
	private JTextField FnameTextField;
	private JTextField LnameTextField;
	private JTextField EmailTextField;
	private JTextField IDTextField;
	private JLabel lblcurrentTimestampz;
	private static Employee employee;
	private JTextField ANITextField;
	private static String inputUUI;
	private static CheckSession checkSession;
	
	/**
	 * Java 1.6
	 */
	public static void main(String[] args){
		try {
		
		// if args is not null and length is greater than zero, 
		// assign the element [ID;ANI] of array to string inputUUI, otherwise assign empty
		inputUUI = args != null && args.length>0 ? args[0] : "";
		
		// calls method to check Process ID
		checkSession = new CheckSession();
		
		// if the previous Process ID is running, returns true
		if(checkSession.CheckIfRunning()) {
		
		// kill previous Screenpop session
		checkSession.KillPrevious();
		
		}
		
		// uncomment this to hardcode incoming command line arguments, for testing purposes only 
		//inputUUI = "255959;undefined";
		
		// Pass UUI values and populate employee variable fields with backend data
		employee = new Employee(inputUUI);
		
		// Log Process ID of the current Screenpop to log file
		checkSession.LogProcess();
		
		}catch (IOException err) {
			 System.err.println("Try running Avaya one x agent with admin privileges.");
			 System.err.println(err);
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
					ScreenPop window = new ScreenPop();
					window.frmEmployeeData.setVisible(true);				 
			}
		});
	}

	/**
	 * Create the GUI application.
	 */
	public ScreenPop() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEmployeeData = new JFrame();
		
		frmEmployeeData.setIconImage(Toolkit.getDefaultToolkit().getImage(ScreenPop.class.getResource("/img/manulife_icon.jpg")));
		frmEmployeeData.setTitle("Employee Information");
		frmEmployeeData.setBounds(100, 100, 558, 378);
		frmEmployeeData.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEmployeeData.getContentPane().setLayout(null);
		
		FnameTextField = new JTextField();
		FnameTextField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		FnameTextField.setBounds(158, 125, 259, 24);
		frmEmployeeData.getContentPane().add(FnameTextField);
		FnameTextField.setColumns(10);
		FnameTextField.setText(employee.getFirstName());
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(32, 157, 81, 24);
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frmEmployeeData.getContentPane().add(lblLastName);
		
		LnameTextField = new JTextField();
		LnameTextField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		LnameTextField.setBounds(158, 159, 259, 24);
		LnameTextField.setColumns(10);
		frmEmployeeData.getContentPane().add(LnameTextField);
		LnameTextField.setText(employee.getLastName());
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(32, 123, 81, 24);
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frmEmployeeData.getContentPane().add(lblFirstName);
		
		JLabel lblInternetEmail = new JLabel("Email");
		lblInternetEmail.setBounds(32, 191, 61, 24);
		lblInternetEmail.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frmEmployeeData.getContentPane().add(lblInternetEmail);
		
		EmailTextField = new JTextField();
		EmailTextField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		EmailTextField.setBounds(158, 193, 259, 24);
		EmailTextField.setColumns(10);
		frmEmployeeData.getContentPane().add(EmailTextField);
		EmailTextField.setText(employee.getEmail());
		
		JLabel lblCorperateId = new JLabel("Corperate ID");
		lblCorperateId.setBounds(32, 225, 107, 24);
		lblCorperateId.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frmEmployeeData.getContentPane().add(lblCorperateId);
		
		IDTextField = new JTextField();
		IDTextField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		IDTextField.setBounds(158, 227, 259, 24);
		IDTextField.setColumns(10);
		frmEmployeeData.getContentPane().add(IDTextField);
		IDTextField.setText(employee.getID());
		
		JButton copyFirstName = new JButton("Copy");
		copyFirstName.setBounds(436, 125, 85, 25);
		copyFirstName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CopyToClipboard(FnameTextField.getText());
			}
		});
		copyFirstName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frmEmployeeData.getContentPane().add(copyFirstName);
		
		JButton copyLastName = new JButton("Copy");
		copyLastName.setBounds(436, 160, 85, 25);
		copyLastName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CopyToClipboard(LnameTextField.getText());
			}
		});
		copyLastName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frmEmployeeData.getContentPane().add(copyLastName);
		
		JButton copyInternetEmail = new JButton("Copy");
		copyInternetEmail.setBounds(436, 192, 85, 25);
		copyInternetEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CopyToClipboard(EmailTextField.getText());
			}
		});
		copyInternetEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frmEmployeeData.getContentPane().add(copyInternetEmail);
		
		JButton copyCorperateID = new JButton("Copy");
		copyCorperateID.setBounds(436, 228, 85, 24);
		copyCorperateID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CopyToClipboard(IDTextField.getText());
			}
		});
		copyCorperateID.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frmEmployeeData.getContentPane().add(copyCorperateID);
		
		lblcurrentTimestampz = new JLabel("");
		lblcurrentTimestampz.setForeground(new Color(0, 102, 51));
		lblcurrentTimestampz.setFont(new Font("Verdana", Font.PLAIN, 19));
		lblcurrentTimestampz.setBounds(255, 46, 266, 24);
		frmEmployeeData.getContentPane().add(lblcurrentTimestampz);
		
		JLabel lblAni = new JLabel("Caller Number");
		lblAni.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblAni.setBounds(31, 259, 108, 24);
		frmEmployeeData.getContentPane().add(lblAni);
		
		ANITextField = new JTextField();
		ANITextField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		ANITextField.setText(employee.getANI());
		ANITextField.setColumns(10);
		ANITextField.setBounds(158, 261, 259, 24);
		frmEmployeeData.getContentPane().add(ANITextField);
		
		JButton copyANI = new JButton("Copy");
		copyANI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CopyToClipboard(ANITextField.getText());
			}
		});
		copyANI.setFont(new Font("Tahoma", Font.PLAIN, 15));
		copyANI.setBounds(436, 262, 85, 25);
		frmEmployeeData.getContentPane().add(copyANI);
		
		JLabel lblLogo = new JLabel("");
		ImageIcon imageIcon = new ImageIcon(new ImageIcon(ScreenPop.class.getResource("/img/manulife-logo.png")).getImage().getScaledInstance(165, 71, Image.SCALE_SMOOTH));
		lblLogo.setIcon(imageIcon);
		lblLogo.setBounds(10, 22, 165, 62);
		frmEmployeeData.getContentPane().add(lblLogo);
		
		// Activates JLabel in case of database connection failure
		JLabel lblError = (employee.getFirstName()).equals("Error fetching data!")?new JLabel("Please check SQLError.log for more info."):new JLabel("");
		lblError.setFont(new Font("Verdana", Font.PLAIN, 10));
		lblError.setHorizontalAlignment(SwingConstants.RIGHT);
		lblError.setForeground(Color.RED);
		lblError.setBounds(207, 94, 299, 13);
		frmEmployeeData.getContentPane().add(lblError);
	
		Timer timer = new Timer(500, new ActionListener() {
				@Override
					public void actionPerformed(ActionEvent e) {
						TickTock();	
					}
            });
		timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
	}
		
	// Copy from Textfield to Clipboard
	private void CopyToClipboard(String toCopy) {
		StringSelection stringSelection = new StringSelection(toCopy);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
	}
	
	// set Timestampz to current DateTime instance
	private void TickTock() {
		lblcurrentTimestampz.setText(DateFormat.getDateTimeInstance().format(new Date()));
    }
}
