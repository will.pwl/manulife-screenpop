package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import com.sun.jna.platform.win32.Kernel32;

final public class CheckSession {
	
	// Constructor checks and creates CurrentSessionID.log if doesn't exists.
	public CheckSession() throws IOException {
		File tempFile = new File("CurrentSessionPID.log");
		if(!tempFile.exists()) {
			BufferedWriter writer = new BufferedWriter(new FileWriter("CurrentSessionPID.log"));
			writer.close();
		}
	}

	/* 
	 *Checks and returns true if (the previous screenpop's Process ID is running && is of type java)
	*/
	public Boolean CheckIfRunning() throws IOException {
		String temp;
		Boolean checkIfRunning = false;
		
		/* 
		 * create tasklist command to check if the logged Process ID is running
		 * Returns output from command line if can't find the PID task running: 
		 * "INFO: No tasks are running which match the specified criteria."	
		 */
		String command = "tasklist /fi \"pid eq " +  ReadLog() + "\"";

		// execute command
		Process process = Runtime.getRuntime().exec(command);
		
		// Buffered reader to get the output from the command line
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		
		while ((temp = reader.readLine()) != null) {
		    if(temp.contains("javaw.exe") || temp.contains("java.exe")) {	    	
		    	
				checkIfRunning = true;
				break;
		    }
		}
		reader.close();
		
		return checkIfRunning;
	}
	
	// Read CurrentSessionPID.log to return the PID of the previously logged Process ID
	private String ReadLog() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader("CurrentSessionPID.log"));

		String PID = reader.readLine();

		reader.close();

		return PID;
	}

	// Runs a command line to kill the previous screenpop according to its logged Process ID
	public void KillPrevious() throws IOException {

		// create command to kill the previously opened screenpop
		String command = "taskkill /F /PID " + ReadLog();

		// execute command
		Runtime.getRuntime().exec(command);
	}

	// Log process of the current Process ID to CurrentSessionPID.log file
	public void LogProcess() throws IOException {

		Integer pid = Kernel32.INSTANCE.GetCurrentProcessId();

		BufferedWriter writer = new BufferedWriter(new FileWriter("CurrentSessionPID.log"));

		writer.write(pid.toString());

		writer.close();

	}
}
